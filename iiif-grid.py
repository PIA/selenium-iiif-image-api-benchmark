""" This script uses selenium to open the e-manuscripta website, search for a term and download the images."""
import time

from selenium import webdriver
from selenium.webdriver import Chrome
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager
import random

# start by defining the options
options = webdriver.ChromeOptions()
#options.add_experimental_option("detach", True)

# normally, selenium waits for all resources to download
# we don't need it as the page also populated with the running javascript code.
options.page_load_strategy = 'none'

# This returns the path web driver downloaded
chrome_path = ChromeDriverManager().install()
chrome_service = Service(chrome_path)

# Pass the defined options and service objects to initialize the web driver
driver = Chrome(options=options, service=chrome_service)
# driver.maximize_window()
driver.implicitly_wait(5)

# Open grid page from local dev env
url = f"http://localhost:5173/grid?type=9"
driver.get(url)
time.sleep(4)

# collect items in here
images = []

# scroll down a few times to stress loading of grid images
for _i in range(50):
    print("round {} of 50".format(_i))
    for _j in range(random.randrange(2, 5)):
        driver.execute_script("window.scrollBy({ top: document.body.scrollHeight, left:0, behavior: 'smooth' })");
    images = driver.find_elements(By.CSS_SELECTOR, "a.block")
    time.sleep(random.randrange(5, 7))
    target = random.choice(images)
    target.click()
    time.sleep(7)
    driver.back()
    time.sleep(5)


# load grid
# scroll a few times
# pause and wait for all images until they download
# instrument and save how long each image took to load, save with canonical url
# reload page and scroll

print("Done!")
