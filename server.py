from flask import Flask, make_response, request, jsonify
import io
import csv

app = Flask(__name__)

all = []

@app.route('/report', methods=['POST'])
def generate_report():
    try:
        # Get the JSON file from the request body
        json_data = request.get_json(force=True)
        # just append the data
        all.append(json_data)
        return "null", 200

    except Exception as e:
        print(e)
        return jsonify(error=str(e)), 400


@app.route('/file', methods=['GET'])
def get_csv_file():
    output = io.StringIO()
    writer = csv.writer(output)
    writer.writerow(["url", "duration in milliseconds"])
    for row in all:
        writer.writerow([row["url"], row["duration"]])
    o = make_response(output.getvalue())
    o.headers["Content-Disposition"] = "attachment; filename=export.csv"
    o.headers["Content-type"] = "text/csv"
    return o


if __name__ == '__main__':
    app.run(debug=True, port=8080)
